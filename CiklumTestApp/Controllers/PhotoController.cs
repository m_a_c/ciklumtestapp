﻿using CiklumTestApp.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace CiklumTestApp.Controllers
{
    public class PhotoController : Controller
    {
        GalleryContext _context = new GalleryContext();
        int _pageSize = 9;

        public ActionResult Index(int page = 1)
        {
            GalleryViewModel gallery = new GalleryViewModel();
            gallery.Photos = _context.Photos.OrderByDescending(i => i.UploadDate).Skip((page - 1) * _pageSize).Take(_pageSize).ToList();
            gallery.Pagination = new Pagination()
            {
                CurrentPage = page,
                PageSize = _pageSize,
                TotalPages = (int)Math.Ceiling((decimal)_context.Photos.Count() / _pageSize),
                TotalItems = _context.Photos.Count()
            };

            return View(gallery);
        }

        public ActionResult Details(int? id)
        {
            try
            {
                if (id == null)
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                Photo photo = _context.Photos.FirstOrDefault(i => i.Id == id);
                if (photo == null)
                    return HttpNotFound();
                return View(photo);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }

        [Authorize]
        [HttpGet]
        public ActionResult Upload()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Upload(string name, HttpPostedFileBase uploadImage)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(name))
                {
                    ViewBag.NameError = "Enter name";
                    return View();
                }
                if (uploadImage == null)
                {
                    ViewBag.PhotoError = "Choose photo";
                    return View();
                }
                string filename = uploadImage.FileName;
                string photoPath = Path.Combine(Server.MapPath("~/Uploads"), filename);
                string thumbnailPath = Path.Combine(Server.MapPath("~/Uploads/Thumbnails"), filename);
                string shortPhotoPath = string.Format("/Uploads/{0}", filename);
                string shortThumbPath = string.Format("/Uploads/Thumbnails/{0}", filename);

                var file = _context.Photos.FirstOrDefault(i => i.PhotoPath == shortPhotoPath);
                if (file != null)
                {
                    ViewBag.Message = "File already exists";
                    return View();
                }
                uploadImage.SaveAs(photoPath);
                Image newImage = new Bitmap(Image.FromStream(uploadImage.InputStream), new Size(300, 200));
                newImage.Save(thumbnailPath);
                _context.Photos.Add(new Photo()
                {
                    Name = name,
                    PhotoPath = shortPhotoPath,
                    ThumbnailsPath = shortThumbPath,
                    UploadDate = DateTime.Now
                });
                _context.SaveChanges();
                ViewBag.Message = "Success";
                return View();
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }
    }
}