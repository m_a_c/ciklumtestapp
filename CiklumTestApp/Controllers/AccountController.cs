﻿using CiklumTestApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace CiklumTestApp.Controllers
{
    public class AccountController : Controller
    {
        GalleryContext _context = new GalleryContext();
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    UserModel user = _context.Users.FirstOrDefault(u => u.Name == model.Name && u.Password == model.Password);
                    if (user != null)
                    {
                        FormsAuthentication.SetAuthCookie(user.Name, true);
                        return RedirectToAction("Index", "Photo");
                    }
                    ModelState.AddModelError("", "Incorrect name or password");
                }
                return View(model);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }
        
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Photo");
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(RegisterViewModel model)
        {
            if(ModelState.IsValid)
            {
                UserModel user = _context.Users.FirstOrDefault(i => i.Name == model.Name);
                if (user == null)
                {
                    _context.Users.Add(new UserModel() { Name = model.Name, Password = model.Password });
                    _context.SaveChanges();

                    user = _context.Users.FirstOrDefault(i => i.Name == model.Name && i.Password == model.Password);
                    if (user != null)
                    {
                        FormsAuthentication.SetAuthCookie(user.Name, true);
                        return RedirectToAction("Index", "Photo");
                    }
                }
                else ModelState.AddModelError("", "User already exist");           
            }
            return View(model);
        }
    }
}