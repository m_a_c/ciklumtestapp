﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CiklumTestApp.Models
{
    public class GalleryContext : DbContext
    {
        public GalleryContext() : base("GalleryConnection") { }
        public DbSet<Photo> Photos { get; set; }
        public DbSet<UserModel> Users { get; set; }
    }
}