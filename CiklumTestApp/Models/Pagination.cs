﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CiklumTestApp.Models
{
    public class Pagination
    {
        public int PageSize { get; set; }
        public int CurrentPage { get; set; }
        public int TotalItems { get; set; }
        public int TotalPages { get; set; }
    }
}