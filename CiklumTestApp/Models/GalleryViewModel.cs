﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CiklumTestApp.Models
{
    public class GalleryViewModel
    {
        public List<Photo> Photos { get; set; }
        public Pagination Pagination { get; set; }
    }
}